/**
 
(in ubuntu 13.10 for sure, maybe everything)
has to be complied with the following additional option

-Wl,--no-as-needed

https://bugs.launchpad.net/ubuntu/+source/gcc-defaults/+bug/1228201

so:

g++ -std=c++0x -Wl,--no-as-needed -pthread main.cpp -o palindrome-product

**/

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <thread>
#include <mutex>
#include <getopt.h>
#include <ctime>
#include <chrono>

bool verbose = false;
bool listAll = false;
unsigned int numThreads = 1;
unsigned int start = 0;
unsigned int end = 10;

std::mutex palindromesLock;
std::mutex largestPalindromeLock;
std::vector<unsigned int> palindromes;
unsigned int largestPalindrome = 0;

std::string usage()
{
    std::string str = "\n\
This program calculates the  palindromes that are \n\
the product of two integers between the passed indices.\n\
It can also simply show the largest palindrome.\n\n";
    str +=
"usage: palindrome-product <start index> <end index>\n\
usage: palindrome-product --list-all [--threads num] <start index> <end index>\n\n\
example: largest palindrome that's a product of two three digit numbers\n\
palindrome-product 100 999\n\n";
    str +=
"--help -h ...................... prints this help and exits\n\
--threads -t (optional) ........ specify the number of threads to use, default 1\n\
--list-all -l .................. if passed, lists all palindromes found, default print only largest\n\
--verbose  -v .................. if passed, prints a little more useless text\n\n\
<start index> (required) ....... bottom index\n\
<end index> (required) ......... top index\n\n";
    return str;
}

int parseInput(int argc, char* argv[])
{
    // contains the short version of each option passed
    // so it can be switched
    char c;
    // contains index so if long option details need to be
    // referenced, they can be
    int longOptIndex;
    // actual long options and their short representations
    // http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html
    struct option long_options[] =
    {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"list-all", no_argument, 0, 'l'},
        {"threads", required_argument, 0, 't'}
    };

    // only one arg (program name)
    if (argc == 1)
    {
        std::cout << usage();
        return 1;
    }
    else
    {
        // get each short arg representation out of argv
        while ((c = getopt_long(argc, argv, "hvlt:", long_options, &longOptIndex)) != -1)
        {
            // switch the current option
            switch (c)
            {
                case 'h': // help
                    std::cout << usage();
                    return 1;
                    break;
                case 'v': // verbose flag
                    verbose = true;
                    break;
                case 'l': // list-all flag
                    listAll = true;
                    break;
                case 't': // threads
                    numThreads = atoi(optarg);
                    break;
                case '?': // option not recognized or no argument if required
                    std::cout << usage();
                    return 1;
                    break;

            } // switch
        } // while (all the options)
    } // else (argc > 1)

    if (argc - optind != 2)
    {
        std::cout << usage();
        return 1;
    }
    start = atoi(argv[optind++]);
    end = atoi(argv[optind++]);
    if (start > end)
    {
        std::cout << usage();
        return 1;
    }
    return 0;
}

std::string int2str(int num)
{
    std::string str = "";
    if (num < 0)
    {
        str += "-";
        num *= -1;
    }

    int digit;
    bool haveDisplayed = false;
    for (int fac = 1000000000; fac >= 1; fac /= 10)
    {
        digit = num / fac;
        if ((digit != 0 && !haveDisplayed) || (fac == 1 && !haveDisplayed)) haveDisplayed = true;
        if (haveDisplayed)
        {
            switch (digit)
            {
                case 0: str += "0"; break; case 1: str += "1"; break;
                case 2: str += "2"; break; case 3: str += "3"; break;
                case 4: str += "4"; break; case 5: str += "5"; break;
                case 6: str += "6"; break; case 7: str += "7"; break;
                case 8: str += "8"; break; case 9: str += "9"; break;
            }
        }
        num -= fac * digit;
    }
    return str;
}

bool isPalindrome(std::string str)
{
    // first get the first and second half of the string
    // if length of total string is odd, then the middle character
    // is in neither variable, as it doesn't matter
    float len = (float)str.length() / 2.0; // half of length of string
    std::string first =  str.substr(0,         floor(len)); // get first half
    std::string second = str.substr(ceil(len), floor(len)); // get second half

    bool returnValue = true; // assume it is a palindrome

    // iterate the length of each half string
    for (int i = 0; i < floor(len); i++)
    {
        // if the leading char != the tailing char
        // etc. for each iteration
        if ( first.at(i) != second.at(floor(len) - i - 1) )
        {
            returnValue = false;
            break;
        }
        else continue;
    }

    return returnValue;
}

void calculator(int start)
{
    // increment first factor by number of threads to avoid checking things twice
    for (unsigned int first = start; first <= end; first += numThreads)
    {
        // increment second factor by 1
        // avoid rechecks by starting at first factor
        for (unsigned int second = first; second <= end; second += 1)
        {
            if (isPalindrome(int2str(first*second)))
            {
                // wait for vector to be unlocked and then add result to it
                palindromesLock.lock();
                    palindromes.push_back(first*second);
                palindromesLock.unlock();
                // wait for largestPalindrome var to be unlocked and the see if this is bigger
                largestPalindromeLock.lock();
                    if (largestPalindrome < first*second) largestPalindrome = first*second;
                largestPalindromeLock.unlock();
            }
        }
    }

}

int main(int argc, char* argv[])
{
    auto beginningTime = std::chrono::high_resolution_clock::now();


    // parse command line options. if something is wrong,
    // it has displayed usage and it is time to exit
    if (parseInput(argc, argv) != 0) return 1;

    // array to hold thread handles
    std::vector<std::thread> threads;
    // make each thread
    for (unsigned int i = 0; i < numThreads; i++)
    {
        threads.push_back( std::thread(calculator, start+i) );
    }

    if (verbose) std::cout << "Calculating on " << numThreads << " thread" << (numThreads==1?"":"s") << ", please be patient.\n";

    // wait for all threads to finish before displaying results
    for (unsigned int i = 0; i < numThreads; i++)
        threads.at(i).join();

    // display results

    if (listAll)
    {
        if (verbose) std::cout << "These are all the palindromes that are a product of two numbers between " << start << " and " << end << std::endl;
        for (unsigned int i = 0; i < palindromes.size(); i++)
            std::cout << palindromes.at(i) << std::endl;
    }
    else
    {
        if (verbose) std::cout << "This is the largest palindrome that is the product of two numbers between " << start << " and " << end << std::endl;
        std::cout << largestPalindrome << std::endl;
    }

    if (verbose)
    {
        auto endingTime = std::chrono::high_resolution_clock::now();
        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(endingTime - beginningTime).count();

        std::cout << "Wow! That took " << milliseconds/1000.0 << " seconds!\n";
    }
}
